/* GLOBAL VARIABLES */
let sv_list = [];
let edit_id = "";
const add_btn = document.querySelector("#add-btn");
const update_btn = document.querySelector("#update-btn");
const reset_btn = document.querySelector("#reset-btn");
update_btn.disabled = true;
//********** LOCAL STORAGE ********///
const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
function setLocalStorage(sv_list) {
  // Tạo JSON
  let dssvJson = JSON.stringify(sv_list);
  // Lưu JSON vào Local Storage
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
}
const getLocalStorage = () => {
  let dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
  if (dssvJson != null) {
    sv_list = JSON.parse(dssvJson);
    for (let index = 0; index < sv_list.length; index++) {
      let sv = sv_list[index];
      sv_list[index] = new SinhVien(
        sv.id,
        sv.name,
        sv.email,
        sv.password,
        sv.score.math,
        sv.score.physic,
        sv.score.chemistry
      );
    }
    renderDSSV(sv_list);
  }
};
getLocalStorage();
// ADD EVENT //
add_btn.addEventListener("click", addSV);
// UPDATE EVENT//
update_btn.addEventListener("click", updateSinhVien);
// RESET FORM //
reset_btn.addEventListener("click", resetForm);

function addSV() {
  let sv = layThongTinTuForm();

  const isValid = checkValidatedInput(sv);
  const notDuplicated = checkDuplicated(sv, sv_list);
  if (isValid && notDuplicated) {
    sv_list.push(sv);
    setLocalStorage(sv_list);
    resetForm();
    renderDSSV(sv_list);
  }
}
function xoaSinhVien(id) {
  let index = timKiemViTri(id, sv_list);
  if (index != -1) {
    sv_list.splice(index, 1);
    // Update JSON vao Local Storage
    setLocalStorage(sv_list);
    renderDSSV(sv_list);
  }
}

function suaSinhVien(id) {
  resetForm();
  let index = timKiemViTri(id, sv_list);
  console.log("index: ", index);
  if (index != -1) {
    let sv = sv_list[index];
    showThongTinLenForm(sv);
    edit_id = sv.id;
    add_btn.disabled = true;
    update_btn.disabled = false;
  }
}
function updateSinhVien() {
  let sv = layThongTinTuForm();
  let index = timKiemViTri(edit_id, sv_list);
  // const isValidId =
  //   validator.checkEmptyString(sv.id, "#spanMaSv", "ID can't be empty") &&
  //   validator.checkLength(sv.id, "#spanMaSv", "ID can't > 4 digits", 4, 4);

  // const isValidName = validator.checkEmptyString(
  //   sv.name,
  //   "#spanTenSV",
  //   "Name can't be empty"
  // );

  // const isValidEmail =
  //   validator.checkEmptyString(
  //     sv.email,
  //     "#spanEmailSV",
  //     "Email can't be empty"
  //   ) && validator.checkValidEmail(sv.email, "#spanEmailSV", "Invalid Email");

  // const isValidPassWord =
  //   validator.checkEmptyString(
  //     sv.password,
  //     "#spanMatKhau",
  //     "Password can't be empty"
  //   ) &&
  //   validator.checkLength(
  //     sv.password,
  //     "#spanMatKhau",
  //     "Password not in range",
  //     4,
  //     20
  //   );

  // const isValidMathScore =
  //   validator.checkEmptyString(
  //     sv.score.math,
  //     "#spanToan",
  //     "Score can't be empty"
  //   ) &&
  //   validator.checkScore(sv.score.math, "#spanToan", "Score must not > 10");

  // const isValidPhysicScore =
  //   validator.checkEmptyString(
  //     sv.score.physic,
  //     "#spanLy",
  //     "Score can't be empty"
  //   ) &&
  //   validator.checkScore(sv.score.physic, "#spanLy", "Score must not > 10");

  // const isValidChemistryScore =
  //   validator.checkEmptyString(
  //     sv.score.chemistry,
  //     "#spanHoa",
  //     "Score can't be empty"
  //   ) &&
  //   validator.checkScore(sv.score.chemistry, "#spanHoa", "Score must not > 10");

  // const isValid =
  //   isValidId &
  //   isValidName &
  //   isValidEmail &
  //   isValidPassWord &
  //   isValidMathScore &
  //   isValidPhysicScore &
  //   isValidChemistryScore;
  const isValid = checkValidatedInput(sv);
  if (isValid) {
    sv_list[index] = sv;
    setLocalStorage(sv_list);
    resetForm();
    renderDSSV(sv_list);
  }
}
