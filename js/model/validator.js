const validator = {
  checkEmptyString: function (value, idError, message) {
    if (value.length == 0) {
      document.querySelector(idError).innerHTML = message;
      return false;
    } else {
      document.querySelector(idError).innerHTML = "";
      return true;
    }
  },
  checkLength: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.querySelector(idError).innerHTML = message;
      return false;
    } else {
      document.querySelector(idError).innerHTML = "";
      return true;
    }
  },
  checkValidEmail: function (value, idError, message) {
    const regex =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (regex.test(value)) {
      document.querySelector(idError).innerHTML = "";
      return true;
    } else {
      document.querySelector(idError).innerHTML = message;
      return false;
    }
  },
  checkDuplicate: function (value, checklist, idError, message) {
    let index = checklist.findIndex(
      (checkItem) => checkItem.id == value || checkItem.email == value
    );
    if (index == -1) {
      // document.querySelector(idError).innerHTML = "";
      return true;
    } else {
      document.querySelector(idError).innerHTML = message;
      return false;
    }
  },
  checkScore: function (value, idError, message) {
    if (value * 1 > 10) {
      document.querySelector(idError).innerHTML = message;
      return false;
    } else {
      // document.querySelector(idError).innerHTML = "";
      return true;
    }
  },
};
// const regex =
//   /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
