class SinhVien {
  constructor(
    id,
    name,
    email,
    password,
    math_score,
    physic_score,
    chemistry_score
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.score = {
      math: math_score,
      physic: physic_score,
      chemistry: chemistry_score,
      getAverage: function () {
        return (
          (this.math * 1 + this.physic * 1 + this.chemistry * 1) /
          3
        ).toFixed(2);
      },
    };
  }
}
