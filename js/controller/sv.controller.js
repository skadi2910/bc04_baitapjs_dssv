function layThongTinTuForm() {
  const maSV = document.querySelector("#txtMaSv").value;
  const tenSV = document.querySelector("#txtTenSV").value;
  const emailSV = document.querySelector("#txtEmail").value;
  const passwordSv = document.querySelector("#txtPass").value;

  const diemToan = document.querySelector("#txtDiemToan").value;
  const diemLy = document.querySelector("#txtDiemLy").value;
  const diemHoa = document.querySelector("#txtDiemHoa").value;

  return new SinhVien(
    maSV,
    tenSV,
    emailSV,
    passwordSv,
    diemToan,
    diemLy,
    diemHoa
  );
}
const resetForm = () => {
  document.querySelector("#txtMaSV").disabled = false;

  document.querySelector("#txtMaSV").value = "";
  document.querySelector("#txtTenSV").value = "";
  document.querySelector("#txtEmail").value = "";
  document.querySelector("#txtPass").value = "";
  document.querySelector("#txtDiemToan").value = "";
  document.querySelector("#txtDiemLy").value = "";
  document.querySelector("#txtDiemHoa").value = "";

  document.querySelector("#spanMaSv").innerHTML = "";
  document.querySelector("#spanTenSv").innerHTML = "";
  document.querySelector("#spanEmailSv").innerHTML = "";
  document.querySelector("#spanMatKhau").innerHTML = "";
  document.querySelector("#spanToan").innerHTML = "";
  document.querySelector("#spanLy").innerHTML = "";
  document.querySelector("#spanHoa").innerHTML = "";

  document.querySelector("#add-btn").disabled = false;
  document.querySelector("#update-btn").disabled = true;
};
function showThongTinLenForm(sv) {
  document.querySelector("#txtMaSV").disabled = true;
  document.querySelector("#txtMaSV").value = sv.id;
  document.querySelector("#txtTenSV").value = sv.name;
  document.querySelector("#txtEmail").value = sv.email;
  document.querySelector("#txtPass").value = sv.passwordSv;
  document.querySelector("#txtDiemToan").value = sv.score.math;
  document.querySelector("#txtDiemLy").value = sv.score.physic;
  document.querySelector("#txtDiemHoa").value = sv.score.chemistry;
}
function renderDSSV(sv_list) {
  let contentHtml = "";
  for (let i = 0; i < sv_list.length; i++) {
    let sv = sv_list[i];
    let contentTr = `<tr>
    <td>${sv.id}</td>
    <td>${sv.name}</td>
    <td>${sv.email}</td>
    <td>${sv.score.getAverage()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.id
    }')" id="xoa-btn" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${
      sv.id
    }')" class="btn btn-warning">Sửa</button>
    </td>
  </tr>`;
    contentHtml += contentTr;
  }
  document.querySelector("#tbodySinhVien").innerHTML = contentHtml;
}

function timKiemViTri(id, sv_list) {
  for (let i = 0; i < sv_list.length; i++) {
    let current_sv = sv_list[i];
    if (current_sv.id == id || id == undefined) {
      return i;
    }
  }
  return -1;
}
function checkValidatedInput(value) {
  const isValidId =
    validator.checkEmptyString(value.id, "#spanMaSv", "ID can't be empty") &&
    validator.checkLength(value.id, "#spanMaSv", "ID can't > 4 digits", 4, 4);

  const isValidName = validator.checkEmptyString(
    value.name,
    "#spanTenSV",
    "Name can't be empty"
  );

  const isValidEmail =
    validator.checkEmptyString(
      value.email,
      "#spanEmailSV",
      "Email can't be empty"
    ) &&
    validator.checkValidEmail(value.email, "#spanEmailSV", "Invalid Email");

  const isValidPassWord =
    validator.checkEmptyString(
      value.password,
      "#spanMatKhau",
      "Password can't be empty"
    ) &&
    validator.checkLength(
      value.password,
      "#spanMatKhau",
      "Password not in range",
      4,
      20
    );

  const isValidMathScore =
    validator.checkEmptyString(
      value.score.math,
      "#spanToan",
      "Score can't be empty"
    ) &&
    validator.checkScore(value.score.math, "#spanToan", "Score must not > 10");

  const isValidPhysicScore =
    validator.checkEmptyString(
      value.score.physic,
      "#spanLy",
      "Score can't be empty"
    ) &&
    validator.checkScore(value.score.physic, "#spanLy", "Score must not > 10");

  const isValidChemistryScore =
    validator.checkEmptyString(
      value.score.chemistry,
      "#spanHoa",
      "Score can't be empty"
    ) &&
    validator.checkScore(
      value.score.chemistry,
      "#spanHoa",
      "Score must not > 10"
    );

  const isValid =
    isValidId &&
    isValidName &&
    isValidEmail &&
    isValidPassWord &&
    isValidMathScore &&
    isValidPhysicScore &&
    isValidChemistryScore;

  return isValid;
}
function checkDuplicated(value, checklist) {
  const notDuplicatedId = validator.checkDuplicate(
    value.id,
    checklist,
    "#spanMaSv",
    "ID Already used"
  );
  const notDuplicatedEmail = validator.checkDuplicate(
    value.email,
    checklist,
    "#spanEmailSv",
    "Email already used"
  );
  const notDuplicated = notDuplicatedId && notDuplicatedEmail;
  return notDuplicated;
}
